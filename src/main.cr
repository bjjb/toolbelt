require "option_parser"

class Parser < ::OptionParser

  property :name { PROGRAM_NAME }
  property :stdout { STDOUT }
  property :stderr { STDERR }

  property? :quiet { false }
  property? :verbose { false }

  # Add an undocumented flag handler.
  def on(flag : String, &block : String ->)
    flag, type = parse_flag_definition(flag)
    @handlers[flag] = Handler.new(type, block)
  end

  # Override on(String, String, String ->)
  def on(flag : String, desc : String, &block : String ->)
    super(flag, desc, &block)
  end

  # Override on(String, String, String, String ->)
  def on(short : String, long : String, desc : String, &block : String ->)
    super(short, long, desc, &block)
  end

  def puts(*objects)
    stdout.puts(*objects)
  end

  def perr(*objects)
    stderr.puts(*objects)
  end

  def info(*objects)
    puts(*objects) unless quiet?
  end

  def warn(*objects)
    perr(*objects) unless quiet?
  end

  def fatal(*objects)
    perr(*objects)
    exit 1
  end

  # Given an array of 3 words, containing
  #
  # - a program name
  # - a partially completed word
  # - the last fully completed word
  #
  # this method prints out, one per line, the next words which are allowed by
  # this OptionParser.
  #
  # This is a basic Bash-style command-line completion helper. See your
  # shell's `help complete` and the manual for details. It will be invoked by
  # bash if you set `complete -C toolbelt toolbelt`, for example, which says
  # "whenever I try to complete a word after 'toolbelt', call 'toolbelt' with
  # certain environment variables set". (The function uses `$COMP_LINE`, so
  # you can test your completion by invoking the program with that variable
  # set - see bash(1) for details.)
  #
  # TODO: allow flag values and arguments to be completed also.
  # TODO: this uses an internal variable (@handlers) which seems risky. It
  # might be better to copy the implementation and add a more stable API.
  def complete(args : Array(String))
    return unless ENV.has_key?("COMP_LINE") # check we're in completion mode
    words = ENV.fetch("COMP_LINE", "").split(/\s+/)
    return unless words.size > 1 # ignore incomplete lines
    return unless args.size == 3 # fail if the args are incorrect
    return unless args[0] == words.shift # the first arg must be the progname
    return unless args[1] == args[1] # the second arg must be the partial
    # Remaining words are subcommands or flags; we need to reset @handlers (in
    # the order specified on the command-line) so that only applicable matches
    # are available.
    words.each do |word|
      next if word.starts_with?("-") # don't invoke flags,
      return unless @handlers[word]? # fail unless there's a handler...
      return unless @handlers[word].block # ...and it is callable,
      handler = @handlers[word].block # remember this handler...
      @handlers = {} of String => Handler # ... and clear out @handlers...
      handler.call("") # ... and call the handler to set up @handlers again.
    end
    # Finally, print out whatever flags begin with the partially completed
    # word (which is the second argument).
    partial = args[1]
    puts @handlers.keys.select { |s| s.starts_with?(partial) }.join("\n")
  end
end

op = Parser.new do |op|
  op.banner = "Usage: #{PROGRAM_NAME} [opts...] [command] [args...]"

  op.on("-h", "--help", "Print this message") do
    op.puts op
    exit(0)
  end

  op.separator("Available commands:")

  op.on("help", "Print help for a particular topic") do |topic|
    op.banner = "Usage: #{PROGRAM_NAME} help <topic>"
    op.unknown_args do |args, _|
      op.fatal "What do you wanna know? 🤓" if args.empty?
      args.each do |topic|
        op.info "Here's what I know about #{topic}:"
        op.puts "bleep bloop..."
      end
      exit(0)
    end
  end

  # Completion 😎
  op.on("completion") do
    op.banner = "Usage: #{PROGRAM_NAME} completion <shell>"
    op.unknown_args do |shells, rest|
      op.warn "Additional arguments ignored: #{rest}" unless rest.empty?
      op.fatal "You need to specify a shell" if shells.empty?
      exe = PROGRAM_NAME
      shells.each do |shell|
        case shell
        when "bash" then op.puts "complete -C #{exe} #{exe}"
        else
          op.perr "Unknown shell #{shell}"
          exit 1
        end
      end
    end
  end

  # Amazon Web Services ☁️
  op.on("aws", "Work with AWS") do
    op.banner = "Usage: #{PROGRAM_NAME} aws [args...]"
    op.on("-p", "--profile", "Pick a profile") do |p|
      op.puts "You picked profile #{p}"
    end
    cmd = :aws
  end

  # RabbitMQ 🐇
  op.on("rabbit", "Work with RabbitMQ") do
    op.banner = "Usage: #{PROGRAM_NAME} rabbit [args...]"
    cmd = :rabbit
  end

  # PagerDuty 📟
  op.on("pd", "Work with PagerDuty") do
    op.banner = "Usage: #{PROGRAM_NAME} pd [args...]"
  end

  # Postgres 🗄️
  op.on("pg", "Work with Postgres") do
    op.banner = "Usage: #{PROGRAM_NAME} pg [args...]"
    cmd = :postgres
  end

  # Kubernetes ☸️"
  op.on("k8s", "Work with Kubernetes") do
    op.banner = "Usage: #{PROGRAM_NAME} k8s [args...]"
    cmd = :k8s
  end

  # GitLab 🚢
  op.on("gitlab", "Work with GitLab") do
    op.banner = "Usage: #{PROGRAM_NAME} gitlab [args...]"
    cmd = :gitlab
  end

  # Jira 💩
  op.on("jira", "Work with Jira") do
    op.banner = "Usage: #{PROGRAM_NAME} jira [args...]"
    cmd = :jira
  end

  # Doctor ⚕️v
  op.on("doctor", "Check your setup for issues") do
    op.banner = "Usage: #{PROGRAM_NAME} doctor"
    cmd = :doctor
  end

  # Server 🚀
  op.on("start", "Start a server") do
    address : String? = nil
    op.banner = "Usage: #{PROGRAM_NAME} start [args...]"
    op.on("-p ADDR", "--port ADDR", "Set the bind address") do |addr|
      address = addr
    end
    cmd = :server
  end

  op.invalid_option do |flag|
    op.perr "😕 I don't understand `#{flag}`."
    op.perr "Here's what I do know:"
    op.perr
    op.perr op
    exit(1)
  end
end

if ENV.has_key?("COMP_LINE")
  # print "ARGV=#{ARGV.inspect}"
  # puts ENV.select { |k, _| k.starts_with?("COMP_") }.map { |k, v| "#{k}=#{v}" }.join(",")
  op.complete(ARGV)
  exit
else
  op.parse(ARGV)
end
