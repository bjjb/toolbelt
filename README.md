# toolbelt

A command-line helper for developers.

## Installation

### Linux

You can just grab the [latest release][1] for your OS and put it somewhere in
your `$PATH`.

## Usage

It attempts to be self-documenting. See if it's succeeding, starting with:

    toolbelt help

to get started.

## Development

To compile it, you'll need [Crystal][2]. Clone the repo, `cd` into it and run

    shards install
    shards build

That'll get all the dependencies and build the binary (into `bin/toolbelt`).

You can run the tests with

    crystal spec

That's pretty much all you need; if you're careful to write tests _before_ you
write your code, and everthing works, you can probably contribute. 👇

## Contributing

1. Fork it (<https://gitlab.com/bjjb/toolbelt/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Write tests, code and documentation - untested/undocumented code is bad! 😡
4. Commit your changes (`git commit -S`) - unsigned commits are **not** OK! 👿
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new merge request

## Contributors

- [JJ Buckley](https://gitlab.com/bjjb) - creator and maintainer

[1]: https://gitlab.com/bjjb/toolbelt/-/releases
[2]: https://crystal-lang.org
