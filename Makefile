.PHONY: test build image

test:
	@echo "Testing... 🧪"
	@crystal spec

build:
	@echo "Building... 🔮"
	@shards build --production --release

image:
	@echo "Dockerizing... 🐳"
	@docker build -t toolbelt .

