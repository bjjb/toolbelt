# 👷 build stage
FROM crystallang/crystal:1.3.2-alpine AS builder
WORKDIR /build
COPY . .
RUN shards build --production --release --static

# 🚀 final stage
FROM scratch
COPY --from=builder /build/bin/toolbelt /toolbelt
EXPOSE 9000
ENTRYPOINT ["/toolbelt"]
HEALTHCHECK CMD /toolbelt check
